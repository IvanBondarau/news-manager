"""news_manager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

from news_manager import settings
from news_manager_app import views

urlpatterns = [
    path('news-manager/', include('news_manager_app.urls')),
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/register', views.register, name='register'),
    path('accounts/profile/current', views.get_profile_page, name='profile'),
    path('accounts/profile/<int:profile_id>/save', views.save_profile_page, name='save_profile'),
    path('accounts/verify/<str:verification_token>', views.verify, name='verify'),
    path('accounts/verified', views.verified)
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
