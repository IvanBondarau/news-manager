import json

from django.test import TestCase, Client
from django.urls import reverse
from parameterized import parameterized

from news_manager_app.forms import NewsForm
from news_manager_app.models import Tag, News, Author


class UserTest(TestCase):
    fixtures = ['users.json']

    @parameterized.expand([
        ("Valid verification key", "123xxx", 302),
        ("Invalid verification key", "abacaba", 404)
    ])
    def test_verify(self, _, key, result):
        response = self.client.get(reverse('verify', args=[key]))
        assert response.status_code == result


class AuthorTest(TestCase):
    fixtures = ["authors.json"]

    @parameterized.expand([
        ("Valid author id", 1, 200),
        ('Invalid author id', 10, 404)
    ])
    def test_read_author(self, _, author_id, result):
        response = self.client.get(reverse('read_author', args=[author_id]))
        assert result == response.status_code

    @parameterized.expand([
        ("Valid author data",
         {
             "first_name": "Name",
             "last_name": "Surname"
         },
         302),
        ('Invalid author data',
         {},
         422)
    ])
    def test_create_author(self, _, author_data, result):
        response = self.client.post(reverse('create_author'), author_data)
        assert response.status_code == result
        if response.status_code == 302:
            assert 1 == Author.objects.filter(first_name=author_data['first_name'],
                                              last_name=author_data['last_name']).count()

    @parameterized.expand([
        ("Valid author data",
         {
             "first_name": "Name",
             "last_name": "Surname"
         },
         1,
         302),
        ("Invalid author id",
         {
             "first_name": "Name",
             "last_name": "Surname"
         },
         100,
         404),
        ("Invalid author data",
         {
         },
         1,
         422),
    ])
    def test_update_author(self, _, author_data, author_id, result):
        response = self.client.post(reverse('update_author', args=[author_id]), author_data)
        assert response.status_code == result
        if response.status_code == 302:
            assert 1 == Author.objects.filter(first_name=author_data['first_name'],
                                              last_name=author_data['last_name']).count()

    @parameterized.expand([
        ("Valid author id", 1, 302),
        ('Invalid author id', 10, 404)
    ])
    def test_delete_author(self, _, author_id, result):
        response = self.client.get(reverse('delete_author', args=[author_id]))
        assert result == response.status_code


class TagTest(TestCase):
    fixtures = ["tags.json"]

    @parameterized.expand([
        ("Valid tag id", 1, 200),
        ('Invalid tag id', 10, 404)
    ])
    def test_read_tag(self, _, tag_id, result):
        response = self.client.get(reverse('read_tag', args=[tag_id]))
        assert result == response.status_code

    @parameterized.expand([
        ("Valid tag data",
         {
            "name": "Test"
         },
         302),
        ('Invalid tag data',
         {},
         422)
    ])
    def test_create_tag(self, _, tag_data, result):
        response = self.client.post(reverse('create_tag'), tag_data)
        assert response.status_code == result
        if response.status_code == 302:
            assert 1 == Tag.objects.filter(name=tag_data['name']).count()

    @parameterized.expand([
        ("Valid tag data",
         {
             "name": "Test"
         },
         1,
         302),
        ("Invalid tag id",
         {
             "name": "Test"
         },
         100,
         404),
        ('Invalid tag data',
         {},
         1,
         422)
    ])
    def test_update_tag(self, _, tag_data, tag_id, result):
        response = self.client.post(reverse('update_tag', args=[tag_id]), tag_data)
        assert response.status_code == result
        if response.status_code == 302:
            assert 1 == Tag.objects.filter(name=tag_data['name']).count()

    @parameterized.expand([
        ("Valid tag id", 1, 302),
        ('Invalid tag id', 10, 404)
    ])
    def test_delete_tag(self, _, tag_id, result):
        response = self.client.get(reverse('delete_tag', args=[tag_id]))
        assert result == response.status_code
        assert 0 == Tag.objects.filter(pk=tag_id).count()


class NewsTest(TestCase):
    fixtures = ["news.json"]

    @parameterized.expand([
        ("Valid news id", 1, 200),
        ('Invalid news id', 10, 404)
    ])
    def test_read_news(self, _, news_id, result):
        response = self.client.get(reverse('read_news', args=[news_id]))
        assert result == response.status_code

    @parameterized.expand([
        ("Valid news data", True, 200),
        ('Invalid news data', False, 422)
    ])
    def test_create_news(self, _, data_valid, result):
        news = None
        if data_valid:
            news = News.objects.get(pk=1)
        else:
            news = News.objects.get(pk=1)
            news.title = ""
        news.__dict__['author'] = 1
        news.__dict__['tags'] = [1, 2]
        response = self.client.post(reverse('create_news'), news.__dict__)
        assert response.status_code == result

    @parameterized.expand([
        ("Valid news data", True, 1, 302),
        ("Invalid news id", True, 100, 404),
        ('Invalid news data', False, 1, 422)
    ])
    def test_create_news(self, _, data_valid, news_id, result):
        news = None
        if data_valid:
            news = News.objects.get(pk=2)
        else:
            news = News.objects.get(pk=2)
            news.title = ""
        news.__dict__['author'] = 1
        news.__dict__['tags'] = [1, 2]
        response = self.client.post(reverse('update_news', args=[news_id]), news.__dict__)
        assert response.status_code == result

    @parameterized.expand([
        ("Valid news id", 1, 302),
        ('Invalid news id', 10, 404)
    ])
    def test_delete_news(self, _, news_id, result):
        response = self.client.get(reverse('delete_news', args=[news_id]))
        assert result == response.status_code
        assert 0 == News.objects.filter(pk=news_id).count()

