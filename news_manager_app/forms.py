from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.forms import CheckboxSelectMultiple

from news_manager_app.models import Author, Tag, News, UserProfile, UserAccount


class AuthorForm(forms.ModelForm):
    class Meta:
        model = Author
        fields = ['first_name', 'last_name']
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'})
        }


class TagForm(forms.ModelForm):
    class Meta:
        model = Tag
        fields = ['name']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'})
        }


class NewsForm(forms.ModelForm):
    class Meta:
        model = News
        fields = ['title', 'short_text', 'full_text', 'image', 'author', 'tags']

        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'short_text': forms.Textarea(attrs={'class': 'form-control', 'rows': 5}),
            'full_text': forms.Textarea(attrs={'class': 'form-control', 'rows': 10}),
            'author': forms.Select(
                attrs={'class': 'form-control'},
            ),
            'tags': CheckboxSelectMultiple()

        }


class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ['first_name', 'last_name', 'phone', 'address', 'about', 'photo']


class RegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = UserAccount
        fields = ["username", "email", "password1", "password2"]
