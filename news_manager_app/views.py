import logging

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.urls import reverse
from django.utils import timezone

from news_manager_app.forms import AuthorForm, TagForm, NewsForm, RegisterForm, UserProfileForm
from news_manager_app.models import Author, Tag, News, UserAccount

logger = logging.getLogger('django')


def read_author(request, author_id):
    logger.info("Reading author (author_id=%d)" % author_id)
    template = loader.get_template("author.html")

    if Author.objects.filter(pk=author_id).count() == 0:
        logger.error("Reading author error: author with id=%d not found" % author_id)
        return HttpResponseNotFound("Author not found")

    author = Author.objects.get(pk=author_id)
    form = AuthorForm(instance=author)
    context = {
        'id': author_id,
        'form': form
    }

    return HttpResponse(template.render(context, request))


def create_author(request):
    logger.info("Creating new author")
    form = AuthorForm(request.POST)

    if form.is_valid():
        form.save()
        logger.info("Author successfully created (id=%d)" % form.instance.id)
        return HttpResponseRedirect('/news-manager/author')
    else:
        logger.error("Error creating author: invalid data")
        return HttpResponse(status=422)


def update_author(request, author_id):
    logger.info("Updating author (author_id=%d)" % author_id)
    if Author.objects.filter(pk=author_id).count() == 0:
        logger.error("Error updating author: author with id=%d not found" % author_id)
        return HttpResponseNotFound("Author not found")
    form = AuthorForm(request.POST)
    if form.is_valid():
        instance = form.instance
        instance.id = author_id
        instance.save()
        logger.info("Author successfully updated")
        return HttpResponseRedirect('/news-manager/author')
    else:
        logger.error("Error updating author: invalid data")
        return HttpResponse(status=422)


def delete_author(request, author_id):
    logger.info("Deleting author (author_id=%d)" % author_id)
    if Author.objects.filter(pk=author_id).count() == 0:
        logger.error("Error deleting author: author with id=%d not found)" % author_id)
        return HttpResponseNotFound("Author not found")

    Author.objects.filter(id=author_id).delete()

    return HttpResponseRedirect(reverse("authors"))


def find_all_authors(request):
    author_set = Author.objects.all()
    form = AuthorForm()
    template = loader.get_template("author-list.html")
    context = {
        'form': form,
        'authors': author_set
    }
    return HttpResponse(template.render(context, request))


def read_tag(request, tag_id):
    logger.info("Reading tag (tag_id=%d)" % tag_id)
    template = loader.get_template("tag.html")

    if Tag.objects.filter(pk=tag_id).count() == 0:
        logger.error("Reading tag error: tag with id=%d not found" % tag_id)
        return HttpResponseNotFound("Tag not found")

    tag = Tag.objects.get(pk=tag_id)
    form = TagForm(instance=tag)
    context = {
        'id': tag_id,
        'form': form
    }

    return HttpResponse(template.render(context, request))


def create_tag(request):
    logger.info("Creating new tag")
    form = TagForm(request.POST)

    if form.is_valid():
        form.save()
        logger.info("Tag successfully created (id=%d)" % form.instance.id)
        return HttpResponseRedirect('/news-manager/tag')
    else:
        logger.error("Error creating tag: invalid data")
        return HttpResponse(status=422)


def update_tag(request, tag_id):
    logger.info("Updating tag (tag_id=%d)" % tag_id)
    if Tag.objects.filter(pk=tag_id).count() == 0:
        logger.error("Error updating tag: tag with id=%d not found" % tag_id)
        return HttpResponseNotFound("Tag not found")

    form = TagForm(request.POST)

    if form.is_valid():
        instance = form.instance
        instance.id = tag_id
        instance.save()
        logger.info("Tag successfully updated (id=%d)" % form.instance.id)
        return HttpResponseRedirect(reverse('tags'))
    else:
        logger.error("Error updating tag: invalid data")
        return HttpResponse(status=422)


def delete_tag(request, tag_id):
    logger.info("Deleting tag (tag_id=%d)" % tag_id)
    if Tag.objects.filter(pk=tag_id).count() == 0:
        logger.error("Deleting tag error: tag with id=%d not found)" % tag_id)
        return HttpResponseNotFound("Tag not found")

    Tag.objects.filter(id=tag_id).delete()
    return HttpResponseRedirect('/news-manager/tag')


def find_all_tags(request):
    tag_set = Tag.objects.all()
    template = loader.get_template("tag-list.html")
    form = TagForm()
    context = {
        'tags': tag_set,
        'form': form
    }
    return HttpResponse(template.render(context, request))


def read_news(request, news_id):
    logger.info("Reading news (news_id=%d)" % news_id)
    template = loader.get_template("news-detail.html")

    if News.objects.filter(pk=news_id).count() == 0:
        logger.error("Reading news error: news with id=%d not found" % news_id)
        return HttpResponseNotFound("News not found")

    news = News.objects.get(pk=news_id)
    context = {
        'id': news.id,
        'news': news
    }

    return HttpResponse(template.render(context, request))


def get_create_news_page(request):
    template = loader.get_template("news-create.html")

    context = {
        'form': NewsForm()
    }

    return HttpResponse(template.render(context, request))


def create_news(request):
    logger.info("Creating news")
    form = NewsForm(request.POST, request.FILES)

    form.instance.created_date_time = timezone.now()
    form.instance.edited_date_time = timezone.now()

    if form.is_valid():
        form.save()
        logger.info("News successfully created (id=%d)" % form.instance.id)
        return HttpResponseRedirect('/news-manager/news')
    else:
        logger.error("Error updating news: invalid data")
        return HttpResponse(status=422)


def get_update_news_page(request, news_id):
    instance = News.objects.get(pk=news_id)
    form = NewsForm(instance=instance)
    context = {
        'id': news_id,
        'form': form
    }

    template = loader.get_template('news-edit.html')
    return HttpResponse(template.render(context, request))


def update_news(request, news_id):
    logger.info("Updating news (news_id=%d)" % news_id)
    if News.objects.filter(pk=news_id).count() == 0:
        logger.error("Error updating news: news with id=%d not found" % news_id)
        return HttpResponseNotFound("News not found")

    form = NewsForm(request.POST, request.FILES)

    if form.is_valid():
        previous_instance = News.objects.get(id=news_id)
        form.instance.created_date_time = previous_instance.created_date_time
        form.instance.edited_date_time = timezone.now()
        form.instance.id = news_id
        form.save()
        logger.info("News successfully updated (id=%d)" % form.instance.id)
        return HttpResponseRedirect('/news-manager/news')
    else:
        logger.error("Error updating news: invalid data")
        return HttpResponse(status=422)


def delete_news(request, news_id):
    logger.info("Deleting news (news_id=%d)" % news_id)
    if News.objects.filter(pk=news_id).count() == 0:
        logger.error("Deleting news error: news with id=%d not found)" % news_id)
        return HttpResponseNotFound("News not found")

    News.objects.get(id=news_id).delete()
    return HttpResponseRedirect('/news-manager/news')


def find_all_news(request):
    news = News.objects.all()
    template = loader.get_template('news-list.html')
    context = {
        'news_list': news
    }
    return HttpResponse(template.render(context, request))


def register(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("/accounts/login")
        else:
            return HttpResponse(str(form.errors))
    else:
        form = RegisterForm()
        template = loader.get_template('registration/registration.html')
        return HttpResponse(template.render(
            {
                'form': form
            },
            request
        ))


@login_required
def get_profile_page(request):
    account = UserAccount.objects.get_by_natural_key(username=request.user.username)
    form = UserProfileForm(instance=account.userprofile)
    template = loader.get_template('registration/profile.html')
    return HttpResponse(template.render(
        {
            'form': form,
            'user': account,
            'id': account.userprofile.id
        },
        request
    ))


@login_required
def save_profile_page(request, profile_id):
    account = UserAccount.objects.get_by_natural_key(username=request.user.username)
    form = UserProfileForm(request.POST, request.FILES)
    if form.is_valid():
        form.instance.id = profile_id
        form.instance.user_id = account.id
        form.save()

        return HttpResponseRedirect('/accounts/profile/current')
    else:
        return HttpResponse(status=422)


def verify(request, verification_token):
    accounts = UserAccount.objects.filter(verification_token=verification_token).all()
    if len(accounts) == 1:
        account = accounts[0]
        account.verified = True
        account.verification_token = None
        account.save()
        return HttpResponseRedirect('/accounts/verified')
    else:
        return HttpResponse(status=404)


def verified(request):
    template = loader.get_template('registration/activated.html')
    return HttpResponse(template.render({}, request))
