from django.urls import path

from . import views

urlpatterns = [
    path('', views.find_all_news, name='home'),
    path('author', views.find_all_authors, name="authors"),
    path('author/<int:author_id>', views.read_author, name='read_author'),
    path('author/<int:author_id>/update', views.update_author, name='update_author'),
    path('author/create', views.create_author, name='create_author'),
    path('author/<int:author_id>/delete', views.delete_author, name='delete_author'),
    path('tag', views.find_all_tags, name="tags"),
    path('tag/create', views.create_tag, name="create_tag"),
    path('tag/<int:tag_id>', views.read_tag, name="read_tag"),
    path('tag/<int:tag_id>/update', views.update_tag, name="update_tag"),
    path('tag/<int:tag_id>/delete', views.delete_tag, name="delete_tag"),
    path('news', views.find_all_news, name="news"),
    path('news/<int:news_id>', views.read_news, name="read_news"),
    path('news/create', views.get_create_news_page, name="get_create_news_page"),
    path('news/create/submit', views.create_news, name="create_news"),
    path('news/<int:news_id>/update', views.get_update_news_page, name="get_update_news_page"),
    path('news/<int:news_id>/update/submit', views.update_news, name="update_news"),
    path('news/<int:news_id>/delete', views.delete_news, name="delete_news")
]
