from django.apps import AppConfig


class NewsManagerAppConfig(AppConfig):
    name = 'news_manager_app'
