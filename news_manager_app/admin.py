import hashlib
import multiprocessing
import random

from django.contrib import admin
from django.core.mail import send_mail



from .models import Tag, Author, News, UserProfile, UserAccount

admin.site.register(Tag)
admin.site.register(Author)
admin.site.register(News)

SERVER_URL = 'http://127.0.0.1:8000/'


def generate_verification_key(account):
    salt = str(hashlib.sha1(str(random.random()).encode('utf8')).hexdigest()[:5]).encode('utf8')
    username_salt = account.username.encode('utf8')
    return hashlib.sha1(salt + username_salt).hexdigest()


def send_confirmation_email(_, __, queryset):
    processes = []
    for account in queryset:
        token = generate_verification_key(account=account)

        account.verification_token = token
        account.save()

        process = multiprocessing.Process(target=send_token, args=(account.username, account.email, token))
        processes.append(process)
        process.start()

        send_token(account.username, account.email, token)

    for process in processes:
        process.join()


def send_token(username, email, token):

    send_mail(
        'Account confirmation',
        'Hello, ' + username + '\n\n'
        + 'To verify your account please follow the link: \n'
        + SERVER_URL + 'accounts/verify/' + token,
        'news.manager.bsuir@gmail.com',
        [email],
        fail_silently=False
    )


class ProfileAdmin(admin.ModelAdmin):
    actions = [send_confirmation_email, ]


admin.site.register(UserProfile)

admin.site.register(UserAccount, ProfileAdmin)
