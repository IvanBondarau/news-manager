from PIL import Image
from django.contrib.auth.models import User, AbstractUser
from django.db import models

# Create your models here.
from django.db.models import CASCADE
from django.db.models.signals import post_save
from django.dispatch import receiver


class Tag(models.Model):
    id = models.AutoField(primary_key=True)

    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Author(models.Model):
    id = models.AutoField(primary_key=True)

    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

    def __str__(self):
        return self.first_name + ' ' + self.last_name


class News(models.Model):
    id = models.AutoField(primary_key=True)

    title = models.CharField(max_length=70)
    short_text = models.CharField(max_length=300)
    full_text = models.CharField(max_length=2000)

    created_date_time = models.DateTimeField()
    edited_date_time = models.DateTimeField()

    image = models.ImageField(default='default-image.jpg', null=True, upload_to='img/')

    author = models.ForeignKey(Author, on_delete=CASCADE)

    tags = models.ManyToManyField(Tag)

    def __str__(self):
        return 'Title = ' + self.title\
                    + ',\nshort_text = ' + self.short_text\
                    + ',\nfull_text = ' + self.full_text

    def save(self, **kwargs):
        super().save()

        img = Image.open(self.image.path)

        if img.height > 800 or img.width > 600:
            output_size = (800, 600)
            img.thumbnail(output_size)
            img.save(self.image.path)


class UserAccount(AbstractUser):
    verified = models.BooleanField(
        default=False
    )

    verification_token = models.CharField(
        max_length=100,
        null=True, unique=True
    )


class UserProfile(models.Model):
    id = models.AutoField(primary_key=True)

    user = models.OneToOneField(UserAccount, on_delete=CASCADE)

    first_name = models.CharField(max_length=100, null=True)
    last_name = models.CharField(max_length=100, null=True)
    phone = models.CharField(max_length=100, null=True)
    address = models.CharField(max_length=300, null=True)

    about = models.CharField(max_length=500, null=True)

    photo = models.ImageField(default='default-image.jpg', null=True, upload_to='img/')

    def save(self, **kwargs):
        super().save()

        img = Image.open(self.photo.path)

        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.photo.path)

    @receiver(post_save, sender=UserAccount)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            UserProfile.objects.create(user=instance)

    @receiver(post_save, sender=UserAccount)
    def save_user_profile(sender, instance, **kwargs):
        instance.userprofile.save()









